/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author Yasser
 */
public class RandomFileBinarySearch {
    public static RandomAccessFile hraf;
    public static int SIZE;
    
    public static int runBinarySearchRecursively(int key, int low, int high) throws IOException {
        if(hraf == null){
            return -1;
        }
        int middle = (low + high) / 2;

        if (high < low) {
            return -1;
        }
        long pos = 8 + SIZE * middle;
        hraf.seek(pos);
        int id = hraf.readInt();
        if (key == id) {
            return middle;
        } else if (key < id) {
            return runBinarySearchRecursively(
                    key, low, middle - 1);
        } else {
            return runBinarySearchRecursively(
                    key, middle + 1, high);
        }
    }
}
